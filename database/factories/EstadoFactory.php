<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Estado;
use Faker\Generator as Faker;

$factory->define(Estado::class, function (Faker $faker) {
    return [
        'nome' => $faker->state,
        'sigla' => $faker->slug(2)
    ];
});
