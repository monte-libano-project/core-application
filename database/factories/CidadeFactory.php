<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Cidade;
use Faker\Generator as Faker;

$factory->define(Cidade::class, function (Faker $faker) {
    $estado = factory(\App\Model\Estado::class)->create();

    return [
        'nome' => $faker->city,
        'codigo_ibge' => $faker->numerify(6),
        'estado_id' => $estado->id
    ];
});
