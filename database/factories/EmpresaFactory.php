<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Empresa;
use Faker\Generator as Faker;

$factory->define(Empresa::class, function (Faker $faker) {
    $cidade = factory(\App\Model\Cidade::class)->create();
    $usuario = factory(\App\Model\Usuario::class)->create();

    return [
        'nome' => $faker->name,
        'cnpj' => $faker->numerify('##.###.###/####-##'),
        'cep' => $faker->numerify('#####-###'),
        'endereco' => $faker->address,
        'numero' => $faker->numberBetween(1, 10000),
        'bairro' => $faker->address,
        'cidade_id' => $cidade->id,
        'responsavel_id' => $usuario->id
    ];
});
