<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EstadoSeeder::class);
        $this->call(CidadeSeeder::class);
        $this->call(UsuarioSeeder::class);
        $this->call(EmpresaSeeder::class);
        // $this->call(PrestadorSeeder::class);
        $this->call(AliquotaImpostoRendaSeeder::class);
    }
}
