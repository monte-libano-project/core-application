<?php

use App\Model\Cidade;
use App\Model\Empresa;
use App\Model\Usuario;
use Illuminate\Database\Seeder;

class EmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Empresa::query()->delete();

        $cidade = Cidade::where('nome', 'Ji-Paraná')->first();
        $usuario = Usuario::where('nome', 'Administrador')->first();

        Empresa::create([
            'cnpj' => '31524146000160',
            'nome' => 'Monte Libano',
            'cep' => '76908455',
            'endereco' => 'Rua Maringá',
            'numero' => '1017',
            'bairro' => 'Nova Brasília',
            'cidade_id' => $cidade->id,
            'responsavel_id' => $usuario->id
        ]);
    }
}
