<?php

use Illuminate\Database\Seeder;

class AliquotaImpostoRendaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('aliquota_imposto_renda')->delete();

        DB::table('aliquota_imposto_renda')->insert([
            [
                'faixa_inicio' => 0,
                'faixa_fim' => 1903.99,
                'aliquota' => 0,
                'deducao' => 0
            ],
            [
                'faixa_inicio' => 1903.99,
                'faixa_fim' => 2826.66,
                'aliquota' => 7.5,
                'deducao' => 142.8
            ],
            [
                'faixa_inicio' => 2826.66,
                'faixa_fim' => 3751.06,
                'aliquota' => 15,
                'deducao' => 354.8
            ],
            [
                'faixa_inicio' => 3751.06,
                'faixa_fim' => 4664.68,
                'aliquota' => 22.5,
                'deducao' => 636.13
            ],
            [
                'faixa_inicio' => 4664.68,
                'faixa_fim' => null,
                'aliquota' => 27.5,
                'deducao' => 869.36
            ]
        ]);
    }
}
