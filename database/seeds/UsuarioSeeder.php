<?php

use App\Model\Usuario;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::query()->delete();
        Usuario::query()->delete();

        Role::create(['name' => 'admin']);
        Role::create(['name' => 'consultor']);

        Usuario::create([
            'nome' => 'Administrador',
            'email' => 'admin@mlibanocontabil.com.br',
            'password' => Hash::make('admin'),
            'email_verified_at' => new \DateTime(),
        ])->assignRole('admin');

        // Usuario::create([
        //     'nome' => 'Consultor',
        //     'email' => 'consultor@mlibano.com.br',
        //     'password' => Hash::make('consultor'),
        //     'email_verified_at' => new \DateTime(),
        // ])->assignRole('consultor');
    }
}
