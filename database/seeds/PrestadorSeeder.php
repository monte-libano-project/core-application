<?php

use App\Model\Cidade;
use App\Model\Empresa;
use App\Model\PrestadorServico;
use Illuminate\Database\Seeder;

class PrestadorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PrestadorServico::query()->delete();

        $cidade = Cidade::where('nome', 'Ariquemes')->first();
        $empresa = Empresa::where('nome', 'Monte Libano')->first();

        PrestadorServico::create([
            'nome' => 'Gabriela Vera Melo',
            'cpf' => '87669924276',
            'rg' => '376399521',
            'rg_orgao_emissor' => 'SSP',
            'inscricao_municipal' => '12345678910',
            'pis_nit' => '123123451212',
            'nome_mae' => 'Tereza Sandra',
            'cep' => '76873472',
            'cidade_id' => $cidade->id,
            'empresa_id' => $empresa->id,
            'endereco' => 'Rua Jasmin',
            'numero' => '560',
            'bairro' => 'Setor 04',
            'telefone' => '6929999926',
            'email' => 'ggabrielaveramelo@ahlstrom.com',
            'data_nascimento' => '1980-10-22',
            'renda_eclesiatica' => false,
        ]);
    }
}
