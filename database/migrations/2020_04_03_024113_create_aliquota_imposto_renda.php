<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAliquotaImpostoRenda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aliquota_imposto_renda', function (Blueprint $table) {
            $table->id();
            $table->decimal('faixa_inicio');
            $table->decimal('faixa_fim')->nullable();
            $table->decimal('aliquota');
            $table->decimal('deducao');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aliquota_imposto_renda');
    }
}
