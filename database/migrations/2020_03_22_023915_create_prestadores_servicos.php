<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrestadoresServicos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestadores_servicos', function (Blueprint $table) {
            $table->id();
            $table->string('cpf', 11)->unique();
            $table->string('rg', 10)->unique();
            $table->string('pis_nit')->unique();
            $table->string('nome');
            $table->string('nome_mae');
            $table->string('rg_orgao_emissor');
            $table->string('inscricao_municipal', 11);
            $table->date('data_nascimento');
            $table->string('cep', 8);
            $table->string('endereco');
            $table->string('numero');
            $table->string('bairro');
            $table->string('email');
            $table->string('telefone', 11);
            $table->boolean('renda_eclesiatica')->default(false);

            $table->unsignedBigInteger('cidade_id');
            $table->foreign('cidade_id')->references('id')->on('cidades');

            $table->unsignedBigInteger('empresa_id');
            $table->foreign('empresa_id')->references('id')->on('empresas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestadores_servicos');
    }
}
