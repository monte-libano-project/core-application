<?php

namespace Tests;

trait AssertionHelpers
{
    /**
     * Asserts that $array1 contains the same values for the keys of $array2.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function assertArraysMatch(array $array1, array $array2)
    {
        foreach (array_keys($array2) as $key) {
            $this->assertEquals($array1[$key], $array2[$key]);
        }
    }
}
