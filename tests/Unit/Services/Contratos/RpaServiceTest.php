<?php

namespace Tests\Unit\Services\Contratos;

use App\Services\Contratos\RpaService;
use PHPUnit\Framework\TestCase;

class RpaServiceTest extends TestCase
{
    /**
     * Calcula corretamente o desconto ISS.
     *
     * @return void
     */
    public function testCalculateDescontoISS()
    {
        $descontoISS = RpaService::calculateDescontoISS(3000);

        $this->assertEquals(150, $descontoISS);
    }

    /**
     * Calcula corretamente o desconto INSS.
     *
     * @return void
     */
    public function testCalculateDescontoINSS()
    {
        $descontoINSS = RpaService::calculateDescontoINSS(3000);

        $this->assertEquals(330, $descontoINSS);
    }

    /**
     * Calcula corretamente o desconto IRPF.
     *
     * @return void
     */
    public function testCalculateDescontoIRPF()
    {
        $descontoIRPF = RpaService::calculateDescontoIRPF(2670, 7.5, 145);

        $this->assertEquals(55.25, $descontoIRPF);
    }
}
