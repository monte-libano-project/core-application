<?php

namespace Tests\Feature\Admin;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\AssertionHelpers;
use Tests\TestCase;

class EmpresasTest extends TestCase
{
    use RefreshDatabase;
    use AssertionHelpers;

    /**
     * Cadastra uma empresa juntamente com o seu usuário responsável.
     *
     * @return void
     */
    public function testCreate()
    {
        $usuario = factory(\App\Model\Usuario::class)->create();
        $cidade = factory(\App\Model\Cidade::class)->create();

        $params = [
            'cnpj' => '12.345.678/0000-00',
            'nome' => 'Empresa 1',
            'cep' => '12345-678',
            'endereco' => 'Rua 1',
            'numero' => '123',
            'bairro' => 'Bairro 1',
            'cidade_id' => $cidade->id,
            'responsavel' => 'João da Silva',
            'telefone' => '(69) 99876-5432',
            'email' => 'joao@example.com',
            'senha' => 'user123'
        ];

        $this->actingAs($usuario, 'web')->post('/admin/empresas', $params);

        $usuario = \App\Model\Usuario::where('email', 'joao@example.com')->first();
        $empresa = \App\Model\Empresa::where('cnpj', '12345678000000')->first();

        $this->assertNotNull($usuario);
        $this->assertNotNull($empresa);

        $this->assertArraysMatch($usuario->toArray(), [
            'nome' => 'João da Silva',
            'telefone' => '69998765432',
            'email' => 'joao@example.com'
        ]);

        $this->assertArraysMatch($empresa->toArray(), [
            'nome' => 'Empresa 1',
            'cep' => '12345678',
            'endereco' => 'Rua 1',
            'numero' => '123',
            'bairro' => 'Bairro 1',
            'cidade_id' => $cidade->id,
            'responsavel_id' => $usuario->id
        ]);
    }

    /**
     * Atualiza uma empresa.
     *
     * @return void
     */
    public function testUpdate()
    {
        $cidade = factory(\App\Model\Cidade::class)->create();
        $usuario = factory(\App\Model\Usuario::class)->create();
        $empresa = factory(\App\Model\Empresa::class)->create();

        $params = [
            'cnpj' => '12.345.678/0000-00',
            'nome' => 'Empresa 1',
            'cep' => '12345-678',
            'endereco' => 'Rua 1',
            'numero' => '123',
            'bairro' => 'Bairro 1',
            'cidade_id' => $cidade->id,
            'responsavel_id' => $usuario->id
        ];

        $this
            ->actingAs($usuario, 'web')
            ->put('/admin/empresas/' . $empresa->id, $params)
            ->assertStatus(302);

        $updated = \App\Model\Empresa::find($empresa->id)->toArray();

        $expected = array_merge($params, ['cnpj' => '12345678000000', 'cep' => '12345678']);

        $this->assertArraysMatch($updated, $expected);
    }
}
