// jQuery masks for common input fields.

$('#cnpj').mask('00.000.000/0000-00')

$('#cep').mask('00000-000')

$('#rg').mask('0000000')

$('#numero').mask('000000')

$('#cpf').mask('000.000.000-00')

$('#pis_nit').mask('000.00000.00-00')


const telefoneMaskBehavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
telefoneMaskOptions = {
    onKeyPress: function(_value, _event, field, options) {
        field.mask(telefoneMaskBehavior.apply({}, arguments), options);
    }
};

$('#telefone').mask(telefoneMaskBehavior, telefoneMaskOptions)
