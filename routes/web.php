<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false, 'reset' => false, 'verify' => false]);

Route::group(['middleware' => ['auth', 'verified']], function () {
    Route::get('/', function () {
        return redirect(route('home'));
    });

    Route::get('/home', 'ApplicationController@index')->name('home');

    Route::resource('/prestadores_servicos', '\App\Http\Controllers\PrestadorServicoController');
    Route::get('/prestadores_servicos/buscar', '\App\Http\Controllers\PrestadorServicoController@search')->name('prestadores_servicos.search');

    Route::get('/contratos', '\App\Http\Controllers\App\ContratoController@index');
    Route::get('/contratos/create', '\App\Http\Controllers\App\ContratoController@create')->name('contratos.create');

    Route::group([], function () {
        Route::get('/admin', 'AdminController@index')->name('admin');

        Route::group(['prefix' => 'admin'], function () {
            Route::resource('/empresas', '\App\Http\Controllers\Admin\EmpresaController')->except(['show']);
            Route::get('/empresas/buscar', '\App\Http\Controllers\Admin\EmpresaController@search')->name('empresas.search');

            Route::resource('/usuarios', '\App\Http\Controllers\Admin\UsuarioController')->except(['show']);
            Route::get('/usuarios/buscar', '\App\Http\Controllers\Admin\UsuarioController@search')->name('usuarios.search');

            Route::resource('/aliquota_imposto_renda', '\App\Http\Controllers\Admin\AliquotaImpostoRendaController')->except(['show']);
        });
    });
});
