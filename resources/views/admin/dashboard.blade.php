@extends('layouts.admin')
@section('title','Dashboard')
@section('main')
<section class="content-header">
	<h1>
		Dashboard
		<small>Painel de controle</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-3 col-xs-6">
			<div class="small-box bg-green">
				<div class="inner">
					<h3>{{ $empresasCount }}</h3>
					<p>Empresas</p>
				</div>
				<div class="icon">
					<i class="fa fa-building"></i>
				</div>
				<a href="{{url('/admin/empresas')}}" class="small-box-footer">
					Detalhes <i class="fa fa-arrow-circle-right"></i>
				</a>
			</div>
		</div>

		<div class="col-lg-3 col-xs-6">
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3>{{ $usuariosCount }}</h3>
					<p>Usuários</p>
				</div>
				<div class="icon">
					<i class="fa fa-user"></i>
				</div>
				<a href="{{url('/admin/usuarios')}}" class="small-box-footer">
					Detalhes <i class="fa fa-arrow-circle-right"></i>
				</a>
			</div>
		</div>
	</div>
</section>
@endsection
