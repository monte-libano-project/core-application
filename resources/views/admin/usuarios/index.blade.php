@extends('layouts.admin')
@section('main')
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<h3>Usuários</h3>
			@if (session('status'))
			<div class="alert alert-success">
				{{ session('status') }}
			</div>
			@endif
		</div>
		<div class="box-body">
			<div style="display: flex;">
				<!-- <a href="{{route('usuarios.create')}}" class="btn btn-success" style="margin-right: 5px">
					<i class="fa fa-plus fa-md" aria-hidden="true"></i> Cadastrar
				</a> -->

				<form action="{{route('usuarios.search')}}" method="GET" role="search">
					{{ csrf_field() }}
					<div class="input-group">
						<input type="text" class="form-control" name="q" placeholder="Buscar usuários" value="{{ $query }}">

						<span class="input-group-btn">
							<button type="submit" class="btn btn-default">
								<span class="fa fa-search"></span>
							</button>
						</span>
					</div>
				</form>
			</div>

			@if(count($usuarios))
			<table class="table table-hover" style="margin-top: 20px">
				<thead>
					<th>#</th>
					<th>Nome</th>
					<th>E-mail</th>
					<th>Função</th>
					<th>Criado em</th>
					<th>Ações</th>
				</thead>
				<tbody>
					@foreach($usuarios as $index => $usuario)
					<tr>
						<td>{{ $index + 1 + $perPage * ($page - 1) }}</td>
						<td>{{ $usuario->nome }}</td>
						<td>{{ $usuario->email }}</td>
						<td>
							@foreach($usuario->roles as $role)
							<span>{{ $role->name }}</span>
							@endforeach
						</td>
						<td>{{ $usuario->created_at->format('d/m/Y H:i') }}</td>
						<td>
							<!-- {{ Form::open(['route' => ['usuarios.destroy', $usuario->id], 'method' => 'delete']) }} -->
							<div class='btn-group'>
								<a href="{{ route('usuarios.edit', [$usuario->id]) }}" class='btn btn-primary btn-sm'>
									<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
								</a>
								<!-- {{
									Form::button(
										'<i class="fa fa-trash-o" aria-hidden="true"></i>',
										['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Você tem certeza?')"]
									)
								}} -->
							</div>
							<!-- {{ Form::close() }} -->
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			<div style="text-align: center">
				{{ $usuarios->render() }}
			</div>

			@else
			<div style="margin-top: 20px; text-align: center">
				<h4>Nenhuma informação encontrada.</h4>
			</div>

			@endif
		</div>
	</div>
</section>
@endsection
