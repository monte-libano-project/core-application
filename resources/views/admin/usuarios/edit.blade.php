@extends('layouts.admin')
@section('main')
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<h3>Editar usuário ({{ $usuario->nome }})</h3>
		</div>
		<div class="box-body">
			<form action="{{route('usuarios.update', [$usuario->id])}}" method="post">
				@method('PUT')
				@csrf
				<input type="hidden" name="usuario_id" value="{{ $usuario->id }}">

				<div class="row">
					<div class="form-group col-sm-6">
						<label for="">Nome</label>
						<input type="text" name="nome" value="{{ $usuario->nome }}" class="form-control">
						@error('nome')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="row">
					<div class="form-group col-sm-6">
						<label for="">E-mail</label>
						<input type="email" name="email" value="{{ $usuario->email }}" id="email" class="form-control">
						@error('email')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="row">
					<div class="form-group col-sm-6">
						<label for="telefone">Telefone</label>
						<input type="text" name="telefone" value="{{ $usuario->telefone }}" id="telefone" class="form-control" placeholder="(00) 0000-0000">
						@error('telefone')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="row">
					<div class="form-group col-sm-6">
						<label for="">Nova senha</label>
						<input type="password" name="senha" class="form-control" placeholder="Senha">
						@error('senha')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="form-group">
					<button class="btn btn-primary" type="submit">Salvar</button>

					<a href="{{url('/admin/usuarios')}}">
						<button class="btn btn-default" type="button">Cancelar</button>
					</a>
				</div>
			</form>
		</div>
	</div>

	<script src="{{ URL::asset('js/InputMasks.js') }}"></script>
</section>
@endsection
