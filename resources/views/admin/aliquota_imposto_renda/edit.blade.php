@extends('layouts.admin')
@section('main')
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<h3>Editar Alíquota IR</h3>
		</div>
		<div class="box-body">
			<form action="{{route('aliquota_imposto_renda.update', [$aliquota->id])}}" method="post">
				@method('PUT')
				@csrf

				<div class="row">
					<div class="form-group col-sm-3">
						<label for="">Faixa - início (R$)</label>
						<input name="faixa_inicio" id="faixa_inicio" value="{{$aliquota->faixa_inicio}}" class="form-control" placeholder="0.00">
						@error('faixa_inicio')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="row">
					<div class="form-group col-sm-3">
						<label for="">Faixa - fim (R$)</label>
						<input name="faixa_fim" id="faixa_fim" value="{{$aliquota->faixa_fim}}" class="form-control" placeholder="0.00">
						@error('faixa_fim')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="row">
					<div class="form-group col-sm-3">
						<label for="">Alíquota (%)</label>
						<input type="text" name="aliquota" value="{{$aliquota->aliquota}}" class="form-control" placeholder="0.0">
						@error('aliquota')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="row">
					<div class="form-group col-sm-3">
						<label for="">Parcela a deduzir (R$)</label>
						<input type="text" name="deducao" value="{{$aliquota->deducao}}" class="form-control" placeholder="0.0">
						@error('deducao')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="form-group">
					<button class="btn btn-primary" type="submit">Salvar</button>

					<a href="{{url('/admin/aliquota_imposto_renda')}}">
						<button class="btn btn-default" type="button">Cancelar</button>
					</a>
				</div>
			</form>
		</div>
	</div>
</section>
@endsection
