@extends('layouts.admin')
@section('main')
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<h3>Alíquota do Imposto de Renda</h3>
			@if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
			@endif
		</div>
		<div class="box-body">
			<!-- <div style="display: flex;">
				<a href="{{route('aliquota_imposto_renda.create')}}" class="btn btn-success" style="margin-right: 5px">
					<i class="fa fa-plus fa-md" aria-hidden="true"></i> Cadastrar
				</a>
			</div> -->

			@if(count($aliquotas))
			<table class="table table-hover" style="margin-top: 20px">
				<thead>
					<th>#</th>
					<th>Base de cálculo</th>
					<th>Alíquota</th>
					<th>Parcela a deduzir</th>
					<th>Ações</th>
				</thead>
				<tbody>
					@foreach($aliquotas as $index => $aliquota)
					<tr>
						<td>{{ $index + 1 }}</td>

						@if($aliquota->faixa_fim)
						<td>De R${{ $aliquota->faixa_inicio }} até R${{ $aliquota->faixa_fim }}</td>
						@else
						<td>Acima de R${{ $aliquota->faixa_inicio }}</td>
						@endif

						@if($aliquota->aliquota != 0)
						<td>{{ floor(($aliquota->aliquota*100))/100 }}%</td>
						@else
						<td>isento</td>
						@endif

						@if($aliquota->deducao != 0)
						<td>R${{ $aliquota->deducao }}</td>
						@else
						<td>isento</td>
						@endif

						<td>
							<!-- {{ Form::open(['route' => ['aliquota_imposto_renda.destroy', $aliquota->id], 'method' => 'delete']) }} -->
							<div class='btn-group'>
								<a href="{{ route('aliquota_imposto_renda.edit', [$aliquota->id]) }}" class='btn btn-primary btn-sm'>
									<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
								</a>
								<!-- {{
									Form::button(
										'<i class="fa fa-trash-o" aria-hidden="true"></i>', 
										['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Você tem certeza?')"]
									) 
								}} -->
							</div>
							<!-- {{ Form::close() }} -->
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			@else
			<div style="margin-top: 20px; text-align: center">
				<h4>Nenhuma aliquota encontrada.</h4>
			</div>

			@endif
		</div>
	</div>
</section>
@endsection
