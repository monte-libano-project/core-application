@extends('layouts.admin')
@section('main')
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<h3>Editar Empresa ({{ $empresa->nome }})</h3>
		</div>
		<div class="box-body">
			<form action="{{route('empresas.update', [$empresa->id])}}" method="post">
				@method('PUT')
				@csrf
				<input type="hidden" name="empresa_id" value="{{$empresa->id}}">

				<div class="row">
					<div class="form-group col-sm-4">
						<label for="">CNPJ</label>
						<input readonly type="cnpj" name="cnpj" id="cnpj" value="{{$empresa->cnpj}}" class="form-control" placeholder="Ex.: 00.000.00/0000-00">
						@error('cnpj')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>

					<div class="form-group col-sm-8">
						<label for="">Nome</label>
						<input type="text" name="nome" value="{{$empresa->nome}}" class="form-control" placeholder="Razão social">
						@error('nome')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="row">
					<div class="form-group col-sm-3">
						<label for="cep">CEP</label>
						<input type="text" class="form-control" name="cep" value="{{$empresa->cep}}" id="cep" placeholder="Ex.: 00000-000">
						@error('cep')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
					<div class="form-group col-sm-7">
						<label for="endereco">Endereço</label>
						<input type="text" class="form-control" name="endereco" value="{{$empresa->endereco}}" id="endereco" placeholder="Ex.: Av. Brasil T2.">
						@error('endereco')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
					<div class="form-group col-sm-2">
						<label for="numero">Numero</label>
						<input type="text" class="form-control" name="numero" value="{{$empresa->numero}}" id="numero">
						@error('numero')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="row">
					<div class="form-group col-sm-6">
						<label for="bairro">Bairro</label>
						<input type="text" class="form-control" name="bairro" value="{{$empresa->bairro}}" id="bairro">
						@error('bairro')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
					<div class="form-group col-sm-6">
						<label for="cidade">Cidade</label>
						<select name="cidade" id="" class="form-control">
							<option value="null">...</option>
							@foreach ($cidades as $cidade)
							@if($cidade->id == $empresa->cidade_id)
							<option value="{{$cidade->id}}" selected>{{$cidade->nome}}</option>
							@else
							<option value="{{$cidade->id}}">{{$cidade->nome}}</option>
							@endif
							@endforeach
						</select>
						@error('cidade')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="row">
					<div class="form-group col-sm-6">
						<label for="responsavel">
							Responsável
							(<a href="{{ route('usuarios.edit', $empresa->responsavel_id) }}">Editar usuário</a>)
						</label>
						<select name="responsavel_id" id="" class="form-control">
							<option value="null">...</option>
							@foreach ($usuarios as $usuario)
							@if($usuario->id == $empresa->responsavel_id)
							<option value="{{$usuario->id}}" selected>{{ $usuario->nome }}</option>
							@else
							<option value="{{$usuario->id}}">{{ $usuario->nome }}</option>
							@endif
							@endforeach
						</select>
						@error('responsavel_id')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="form-group">
					<button class="btn btn-primary" type="submit">Salvar</button>

					<a href="{{url('/admin/empresas')}}">
						<button class="btn btn-default" type="button">Cancelar</button>
					</a>
				</div>
			</form>
		</div>
	</div>

	<script src="{{ URL::asset('js/InputMasks.js') }}"></script>
</section>
@endsection
