@extends('layouts.admin')
@section('main')
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<h3>Empresas</h3>
			@if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
			@endif
		</div>
		<div class="box-body">
			<div style="display: flex;">
				<a href="{{route('empresas.create')}}" class="btn btn-success" style="margin-right: 5px">
					<i class="fa fa-plus fa-md" aria-hidden="true"></i> Cadastrar
				</a>

				<form action="{{route('empresas.search')}}" method="GET" role="search">
					{{ csrf_field() }}
					<div class="input-group">
						<input type="text" class="form-control" name="q" placeholder="Buscar empresas" value="{{ $query }}">

						<span class="input-group-btn">
							<button type="submit" class="btn btn-default">
								<span class="fa fa-search"></span>
							</button>
						</span>
					</div>
				</form>
			</div>

			@if(count($empresas))
			<table class="table table-hover" style="margin-top: 20px">
				<thead>
					<th>#</th>
					<th>CNPJ</th>
					<th>Nome</th>
					<th>Criado em</th>
					<th>Ações</th>
				</thead>
				<tbody>
					@foreach($empresas as $index => $empresa)
					<tr>
						<td>{{ $index + 1 + $perPage * ($page - 1) }}</td>
						<td>{{ StringHelpers::formatCpfCnpj($empresa->cnpj) }}</td>
						<td>{{ $empresa->nome }}</td>
						<td>{{ $empresa->created_at->format('d/m/Y H:i') }}</td>
						<td>
							<!-- {{ Form::open(['route' => ['empresas.destroy', $empresa->id], 'method' => 'delete']) }} -->
							<div class='btn-group'>
								<a href="{{ route('empresas.edit', [$empresa->id]) }}" class='btn btn-primary btn-sm'>
									<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
								</a>
								<!-- {{
									Form::button(
										'<i class="fa fa-trash-o" aria-hidden="true"></i>',
										['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Você tem certeza?')"]
									)
								}} -->
							</div>
							<!-- {{ Form::close() }} -->
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			<div style="text-align: center">
				{{ $empresas->render() }}
			</div>

			@else
			<div style="margin-top: 20px; text-align: center">
				<h4>Nenhuma empresa encontrada.</h4>
			</div>

			@endif
		</div>
	</div>
</section>
@endsection
