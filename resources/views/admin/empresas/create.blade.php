@extends('layouts.admin')
@section('main')
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<h3>Cadastrar Empresa</h3>
		</div>
		<div class="box-body">
			<form action="{{ route('empresas.store') }}" method="post">
				@csrf
				<input type="hidden" name="empresa_id">

				<div class="row">
					<div class="form-group col-sm-4">
						<label for="">CNPJ</label>
						<input type="cnpj" name="cnpj" id="cnpj" class="form-control" placeholder="00.000.000/0000-00">
						@error('cnpj')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>

					<div class="form-group col-sm-8">
						<label for="">Nome</label>
						<input type="text" name="nome" class="form-control" placeholder="Razão social">
						@error('nome')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="row">
					<div class="form-group col-sm-3">
						<label for="cep">CEP</label>
						<input type="text" class="form-control" name="cep" id="cep" placeholder="00000-000">
						@error('cep')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
					<div class="form-group col-sm-7">
						<label for="endereco">Endereço</label>
						<input type="text" class="form-control" name="endereco" id="endereco" placeholder="Ex.: Av. Brasil T2.">
						@error('endereco')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
					<div class="form-group col-sm-2">
						<label for="numero">Numero</label>
						<input type="text" class="form-control" name="numero" id="numero">
						@error('numero')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="row">
					<div class="form-group col-sm-6">
						<label for="bairro">Bairro</label>
						<input type="text" class="form-control" name="bairro" id="bairro">
						@error('bairro')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
					<div class="form-group col-sm-6">
						<label for="cidade">Cidade</label>
						<select name="cidade" id="" class="form-control">
							<option value="null">...</option>
							@foreach ($cidades as $cidade)
							<option value="{{$cidade->id}}">{{$cidade->nome}}</option>
							@endforeach
						</select>
						@error('cidade')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="row">
					<div class="form-group col-sm-8">
						<label for="responsavel">Responsável</label>
						<input type="text" class="form-control" name="responsavel" id="responsavel" placeholder="Insira o nome do responsável pela empresa">
						@error('responsavel')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
					<div class="form-group col-sm-4">
						<label for="telefone">Telefone</label>
						<input type="text" class="form-control" name="telefone" id="telefone" placeholder="(00) 0000-0000">
						@error('telefone')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="row">
					<div class="form-group col-sm-8">
						<label for="email">Email</label>
						<input type="email" class="form-control" name="email" id="email">
						@error('email')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
					<div class="form-group col-sm-4">
						<label for="">Senha</label>
						<input type="password" name="senha" class="form-control" placeholder="Senha">
						@error('senha')
						<div class="text-danger">{{ $message }}</div>
						@enderror
					</div>
				</div>

				<div class="form-group">
					<button class="btn btn-primary" type="submit">Salvar</button>

					<a href="{{url('/admin/empresas')}}">
						<button class="btn btn-default" type="button">Cancelar</button>
					</a>
				</div>
			</form>
		</div>
	</div>

	<script src="{{ URL::asset('js/InputMasks.js') }}"></script>
</section>
@endsection
