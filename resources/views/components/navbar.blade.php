<nav class="navbar navbar-expand-lg navbar-dark rounded" style="background-color: #21416f">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navba" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbar">
    <ul class="navbar-nav mr-auto">
      <li>
        <img src="/images/logo.jpeg" width="40px">
      </li>

      <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
        <a class="nav-link" style="color: #fff" href="/">Home </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" style="color: #fff" href="/prestadores_servicos">Prestadores de serviços</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" style="color: #fff" href="/contratos">Contratos</a>
      </li>
      </li>
    </ul>

    @if(Auth::user())
    <form action="{{ url('/logout') }}" method="POST" class="form-inline my-2 my-lg-0">
      @csrf
      <button class="btn btn-dark my-2 my-sm-0" style="background-color: #21416f" type="submit">Sair</button>
    </form>
    @endif
  </div>
</nav>
