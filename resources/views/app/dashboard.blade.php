@extends('app.home')

@section('content')
    <div class="" style="display: grid; grid-template-columns: 45% 45%; grid-gap: 10px;">
        <div class="card border border-primary">
            <div class="card-body">
                <h5 class="card-title">Prestadores de serviços</h5>
                <p class="card=text">
                    Gerenciar os prestadores de serviços da sua empresa.
                </p>
                <a href="/prestadores_servicos" class="btn btn-primary">Acessar</a>  
            </div>
        </div>

        <div class="card border border-primary">
            <div class="card-body">
                <h5 class="card-title">Contratos</h5>
                <p class="card=text">
                    Imprimir contratos.
                </p>
                <a href="/contratos" class="btn btn-primary">Acessar</a>  
            </div>
        </div>
    </div>
@endsection
