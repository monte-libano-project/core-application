@extends('layouts.app')

@section('main')
    <div style="padding: 10px;">
        @hasSection('content')
            @yield('content')
        @endif
    </div>
@endsection
