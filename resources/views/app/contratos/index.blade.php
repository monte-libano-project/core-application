@extends('app.home')

@section('content')
<h3 style="margin:15px 0">Contratos</h3>
<div class="card border">
    <div class="card-body">
        <form action="{{route('contratos.create')}}" method="GET" role="search">
            {{ csrf_field() }}
            <input type="hidden" name="empresa_id" value="{{$empresa->id}}">

            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="prestador_id">Prestador de serviço</label>
                    <select name="prestador_id" id="" class="form-control">
                        <option value="null">...</option>
                        @foreach ($prestadores as $prestador)
                        <option value="{{$prestador->id}}">{{$prestador->nome}}</option>
                        @endforeach
                    </select>
                    @error('prestador_id')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="tipo_contrato">Contrato</label>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="tipo_contrato" id="tipo_contrato" value="rpa" checked>
                        <label class="form-check-label" for="tipo_contrato">
                            Recibo de Pagamento a Autônomo - RPA
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col">
                    <label for="servico">Serviço</label>
                    <textarea class="form-control" name="servico" value="{{old('servico')}}" id="servico" placeholder="Descrição do serviço prestado"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-3">
                    <label for="valor_bruto">Valor bruto (R$)</label>
                    <input class="form-control" name="valor_bruto" value="{{old('valor_bruto')}}" id="valor_bruto" placeholder="1000.00" />
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-primary" type="submit">Imprimir</button>

                <a href="{{url('/')}}">
                    <button class="btn btn-default" type="button">Cancelar</button>
                </a>
            </div>
        </form>
    </div>
</div>
@endsection
