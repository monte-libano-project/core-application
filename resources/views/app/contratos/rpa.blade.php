<html>
    <head>
        <title>RPA</title>
    </head>
    <body>
        <div>
            <img src="images/logo.jpeg" width="120px" alt="LOGO" style="float: left; margin-right: 300px">
            <div style="float: right">
                <span style="color: #04255f; font-weight: bold">Monte Libano Assessoria Contábil</span> <br/>
                Registro CRC/RO 000723/0-4 <br/>
                Fone: 69-3423-5300 // 69-98492-0100 <br/>
                Site: <a href="https://www.mlibanocontabil.com.br" target="_blank">www.mlibanocontabil.com.br</a> <br/>
                E-mail: <a href="mailto: sac@mlibanocontabil.com.br">sac@mlibanocontabil.com.br</a> <br/>
            </div>
        </div>
        <div class="column" style="padding-top: 50px;">
            <div style="text-align: center;">
                <h3>RECIBO DE PAGAMENTO A AUTÔNOMO – RPA</h3>
            </div>

            <p>
                Recebi do(a) {{$empresa->nome}}, CNPJ  {{ StringHelpers::formatCpfCnpj($empresa->cnpj) }}, 
                a importância líquida de R$ {{ money_format('%i', $valorBruto) }}, pela prestação de serviços de {{ $servico }}.
            </p>

            <p style="font-size: 14pt">Discriminação de valores para o pagamento dos serviços prestados:</p>

            <p>Valor Bruto: R$ {{ money_format('%i', $valorBruto) }}</p>
            <p>Desconto ISS (5%  x Valor Bruto): R$ {{ money_format('%i', $descontoISS) }}</p>
            @if($descontoINSS != 0)
            <p>Desconto INSS (11% x Valor Bruto): R$ {{ money_format('%i', $descontoINSS) }}</p>
            @endif
            <p>
                Desconto IRPF ({{ $aliquotaIRPF }}% x 
                @if($descontoINSS != 0)
                [Valor Bruto - INSS] 
                @else
                Valor Bruto
                @endif
                - R$ {{ money_format('%i', $deducaoIRPF) }} de dedução): R$ {{ money_format('%i', $descontoIRPF) }}
            </p>
            <p>Valor Líquido: R$ {{ money_format('%i', $valorLiquido) }}</p>

            <p style="font-size: 14pt">Dados do prestador:</p>

            <table>
                <tr>
                    <td colspan="4">Nome completo: {{$prestador->nome}}</td>
                </tr>
                <tr><td></td></tr>

                <tr>
                    <td colspan="4">Inscrição Municipal: {{$prestador->inscricao_municipal}}</td>
                </tr>
                <tr><td></td></tr>

                <tr>
                    <td width="300px">CPF: {{ StringHelpers::formatCpfCnpj($prestador->cpf) }}</td>
                    <td>Data Nascimento: {{ date_format(date_create($prestador->data_nascimento), "d/m/Y") }}.</td>
                </tr>
                <tr><td></td></tr>
                <tr>
                    <td>RG: {{$prestador->rg}}</td>
                    <td>Órgão Emissor: {{$prestador->rg_orgao_emissor}}</td>
                </tr>
                <tr><td></td></tr>

                <tr>
                    @if($prestador->pis_nit)
                    <td colspan="4">Nº do PIS ou NIT: {{ StringHelpers::formatPisNit($prestador->pis_nit) }}</td>
                    @else
                    <td colspan="4">Nome da Mãe: {{$prestador->nome_mae}}</td>
                    @endif
                </tr>
                <tr><td></td></tr>

                <tr>
                    <td colspan="4">Endereço: {{$prestador->endereco}}, {{$prestador->bairro}}, {{$prestador->numero}} </td>
                </tr>
                <tr><td></td></tr>
            </table>

            <div style="text-align: center;">
                <p> {{ $cidade->nome }}/{{ $estado->sigla }}, {{ $dia }} de {{ $mes }} de {{ $ano }}.</p>
                <p>_______________________________________________________________</p>
                <p>Assinatura</p>
            </div>
        </div>
    </body>
</html>
