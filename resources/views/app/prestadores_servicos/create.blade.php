@extends('app.home')

@section('content')
<h3 style="margin:15px 0">Cadastrar Prestador Serviço</h3>
<div class="card border">
    <div class="card-body">
        <form action="/prestadores_servicos" method="POST">
            @csrf
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" name="nome" value="{{old('nome')}}" id="nome" placeholder="Nome completo">
                    @error('nome')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-sm-3">
                    <label for="cpf">CPF</label>
                    <input type="text" class="form-control" name="cpf" value="{{old('cpf')}}" id="cpf" placeholder="000.000.000-00">
                    @error('cpf')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-sm-3">
                    <label for="dataNas">Data de Nascimento</label>
                    {{ Form::date('data_nascimento', old('dataNas'), ['class' => 'form-control']) }}
                    @error('data_nascimento')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-3">
                    <label for="inscricao_municipal">Inscrição municipal</label>
                    <input type="text" class="form-control" name="inscricao_municipal" value="{{old('inscricao_municipal')}}" id="inscricao_municipal" placeholder="">
                    @error('inscricao_municipal')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-sm-2">
                    <label for="cep">CEP</label>
                    <input type="text" class="form-control" name="cep" value="{{old('cep')}}" id="cep" placeholder="Ex.: 76900-800">
                    @error('cep')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-sm-5">
                    <label for="endereco">Endereço</label>
                    <input type="text" class="form-control" name="endereco" value="{{old('endereco')}}" id="endereco" placeholder="Ex.: Av. Brasil">
                    @error('endereco')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-sm-2">
                    <label for="numero">Numero</label>
                    <input type="text" class="form-control" name="numero" value="{{old('numero')}}" id="numero" placeholder="Ex.: 1000">
                    @error('numero')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="bairro">Bairro</label>
                    <input type="text" class="form-control" name="bairro" value="{{old('bairro')}}" id="bairro">
                    @error('bairro')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-sm-6">
                    <label for="cidade">Cidade</label>
                    <select name="cidade" value="{{old('cidade')}}" id="" class="form-control">
                        <option value="null">...</option>
                        @foreach ($cidades as $cidade)
                        <option value="{{$cidade->id}}">{{$cidade->nome}}</option>
                        @endforeach
                    </select>
                    @error('cidade')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-2">
                    <label for="rg">RG</label>
                    <input type="text" class="form-control" name="rg" value="{{old('rg')}}" id="rg" placeholder="Ex.: 12.345.567-8">
                    @error('rg')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-sm-2">
                    <label for="rg_orgao_emissor">Órgão emissor</label>
                    <input type="text" class="form-control" name="rg_orgao_emissor" value="{{old('rg_orgao_emissor')}}" id="rg_orgao_emissor" placeholder="Ex.: SSP">
                    @error('rg_orgao_emissor')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-sm-3">
                    <label for="pis_nit">PIS/NIT</label>
                    <input type="text" class="form-control" name="pis_nit" value="{{old('pis_nit')}}" id="pis_nit" placeholder="Insira o PIS ou NIT">
                    @error('pis_nit')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-sm-5">
                    <label for="nome_mae">Nome da mãe</label>
                    <input type="nome_mae" class="form-control" name="nome_mae" value="{{old('nome_mae')}}" id="nome_mae" placeholder="Informar caso não houver PIS/NIT">
                    @error('nome_mae')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-5">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" value="{{old('email')}}" id="email" placeholder="nome@exemplo.com">
                    @error('email')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-sm-4">
                    <label for="telefone">Telefone</label>
                    <input type="telefone" class="form-control" name="telefone" value="{{old('telefone')}}" id="telefone" placeholder="Ex.: (00) 0000-0000">
                    @error('telefone')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-sm-3">
                    <label class="form-check-label" for="renda_eclesiatica">Renda eclesiástica?</label>

                    <div style="margin-top: 15px">
                        <div class="form-check form-check-inline">
                            <input checked type="radio" class="form-check-input" value="0" name="renda_eclesiatica" id="is_not_renda_eclesiatica">
                            <label class="form-check-label" for="is_not_renda_eclesiatica">Não</label>
                        </div>

                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" value="1" name="renda_eclesiatica" id="is_renda_eclesiatica">
                            <label class="form-check-label" for="is_renda_eclesiatica">Sim</label>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
            <a href="{{ route('prestadores_servicos.index') }}" class="btn btn-default btn-sm btn-cancel">Cancelar</a>
        </form>
    </div>
</div>
    <script src="{{ URL::asset('js/InputMasks.js') }}"></script>
@endsection
