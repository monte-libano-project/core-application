@extends('app.home')

@section('content')
<h3 style="margin:15px 0">Prestadores de serviços</h3>
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<div class="card border">
    <div class="card-body">
        <div style="display: flex;">
            <a href="{{route('prestadores_servicos.create')}}" class="btn btn-success" style="margin-right: 5px">
                <i class="fa fa-plus fa-md" aria-hidden="true"></i> Cadastrar
            </a>

            <form action="{{route('prestadores_servicos.search')}}" method="GET" role="search">
                {{ csrf_field() }}
                <div class="input-group">
                    <input type="text" class="form-control" name="q" placeholder="Buscar prestadores de serviços" value="{{ $query }}">

                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default">
                            <span class="fa fa-search"></span>
                        </button>
                    </span>
                </div>
            </form>
        </div>

        @if(count($prestadores))
        <table class="table table-hover" style="margin-top: 20px">
            <thead>
                <th>#</th>
                <th>CPF</th>
                <th>Nome</th>
                <th>Criado em</th>
                <th>Ações</th>
            </thead>
            <tbody>
                @foreach($prestadores as $index => $prestador)
                <tr>
                    <td>{{ $index + 1 + $perPage * ($page - 1) }}</td>
                    <td>{{ StringHelpers::formatCpfCnpj($prestador->cpf) }}</td>
                    <td>{{ $prestador->nome }}</td>
                    <td>{{ $prestador->created_at->format('d/m/Y H:i') }}</td>
                    <td>
                        {{ Form::open(['route' => ['prestadores_servicos.destroy', $prestador->id], 'method' => 'delete']) }}
                        <div class='btn-group'>
                            <a href="{{ route('prestadores_servicos.edit', [$prestador->id]) }}" class='btn btn-primary btn-sm'>
                                Editar
                            </a>
                            <!-- {{
                                Form::button(
                                    '<i class="fa fa-trash-o" aria-hidden="true"></i>',
                                    ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Você tem certeza?')"]
                                )
                            }} -->
                        </div>
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div style="text-align: center">
            {{ $prestadores->render() }}
        </div>

        @else
        <div style="margin-top: 20px; text-align: center">
            <h4>Nenhuma informação encontrada.</h4>
        </div>

        @endif
    </div>
</div>
@endsection
