<!DOCTYPE html>
​
<html lang="{{ app()->getLocale() }}" class="default-style">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
​
    <title>Monte Libano</title>
​
    <!-- Main font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900" rel="stylesheet">
​
​
    <!-- Vuetify CSS -->
    <link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">
​
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script src="{{ asset('js/app.js') }}"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>

    <style>
        html, body {
            height: 100%;
        }

        .container {
            min-height: 100%;
        }

        main {
            overflow: auto;
            /* padding-bottom: 100px; */
        }

        footer {
            position: relative;
            margin-top: -200px;
            /* clear: both; */
        }
    </style>
</head>
<body>
    <div class="container" style="margin-top: -35px; min-height: 100%">
        @if(Auth::user())
            @component('components.navbar')@endcomponent
        @endif

        <main role="main" class="bg-light border" style="display: flex; flex-direction: column; ">
            @hasSection('main')
                @yield('main')
            @endif
        </main>

        <div id="app"></div>
    </div>

    <!-- <footer class="page-footer" style="background-color: #21416f">
        <img src="/images/logo.jpeg">
    </footer> -->
</body>
</html>
