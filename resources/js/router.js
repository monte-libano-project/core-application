import Vue from 'vue';
import Router from 'vue-router';

import Dashboard from './components/Dashboard'
import Empresa from './components/Empresa'
import PrestadorServico from './components/PrestadorServico'

Vue.use(Router);

let routes = [
    {
        path: '/',
        component: Dashboard,
    },
    {
        path: '/empresa',
        component: Empresa,
    },
    {
        path: '/prestadorservico',
        component: PrestadorServico,
    }

];

const router = new Router({
    base: '/',
    mode: 'history',
    routes,
});

export default router;
