<?php

namespace App\Services\Contratos;

class RpaService
{
    public static function calculateDescontoISS($valor)
    {
        return $valor * 5 / 100;
    }

    public static function calculateDescontoINSS($valor)
    {
        return $valor * 11 / 100;
    }

    public static function calculateDescontoIRPF($valor, $aliquota, $deducao)
    {
        return ($valor * $aliquota / 100) - $deducao;
    }

    public static function getAliquotaAndDeducaoFromTabelaIR($valor)
    {
        $tabelaIR = \App\Model\AliquotaImpostoRenda::orderBy('faixa_inicio', 'desc')->get();

        $aliquota = 0;
        $deducao = 0;

        foreach ($tabelaIR as $item) {
            if ($valor >= $item->faixa_inicio) {
                $aliquota = $item->aliquota;
                $deducao = $item->deducao;

                break;
            }
        }

        return [$aliquota, $deducao];
    }
}
