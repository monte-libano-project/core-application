<?php

namespace App\Helpers;

class StringHelpers
{
    public static function formatCpfCnpj($value)
    {
        $cpfCnpj = preg_replace("/\D/", '', $value);

        if (strlen($cpfCnpj) === 11) {
            return preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cpfCnpj);
        }

        return preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cpfCnpj);
    }

    public static function formatPisNit($value)
    {
        $pisNit = preg_replace("/\D/", '', $value);

        return preg_replace("/(\d{3})(\d{5})(\d{2})(\d{2})/", "\$1.\$2.\$3-\$4", $pisNit);
    }
}
