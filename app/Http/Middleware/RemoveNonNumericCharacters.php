<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Remove "special" chracters from some common
 * fields used in application forms, like
 * 'cnpj', 'cpf' and 'phone', leaving only
 * digits.
 */
class RemoveNonNumericCharacters
{
    protected $fields = [
        'cnpj',
        'cep',
        'cpf',
        'telefone'
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        foreach ($this->fields as $field) {
            if ($request->has($field)) {
                $request->merge([
                    $field => $this->numbersOnly(request($field))
                ]);
            }
        }

        return $next($request);
    }

    private function numbersOnly($string) {
        return preg_replace('/\D/', '', $string);
    }
}
