<?php

namespace App\Http\Controllers;

use App\Model\Empresa;
use App\Model\PrestadorServico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PrestadorServicoController extends Controller
{
    protected $perPage = 20;

    /**
     * Display a paginated listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = $this->perPage;
        $page = $request->page ? $request->page : 1;
        $prestadores = PrestadorServico::paginate($this->perPage);

        return view('app.prestadores_servicos.index', compact('prestadores', 'page', 'perPage'))->with('query', $request->q);
    }

    /**
     * Display a filtered paginated listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $perPage = $this->perPage;
        $page = $request->page ? $request->page : 1;
        $query = '%' . $request->q . '%';

        $prestadores_servicos = PrestadorServico::where('cpf', 'LIKE', $query)
            ->orWhere('nome', 'LIKE', $query)
            ->paginate($this->perPage);

        return view('app.prestadores_servicos.index', compact('prestadores_servicos', 'page', 'perPage'))->with('query', $request->q);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cidades = \App\Model\Cidade::where('estado_id', 22)->get();

        return view('app.prestadores_servicos.create', compact('cidades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'nome' => 'required|string|max:255',
            'cpf' => 'unique:prestadores_servicos|required|string|size:11',
            'rg' => 'required|string|max:8',
            'rg_orgao_emissor' => 'required|string|max:8',
            'inscricao_municipal' => 'required|string|size:11',
            'pis_nit' => 'nullable|string|min:11',
            'nome_mae' => 'nullable|string',
            'cep' => 'required|string|size:8',
            'cidade' => 'required|integer',
            'endereco' => 'required|string|max:255',
            'numero' => 'required|string|max:6',
            'bairro' => 'required|string|max:255',
            'telefone' => 'required|string|between:10,11',
            'email' => 'required',
            'data_nascimento' => 'required'
        ]);

        $prestador = new PrestadorServico();

        $prestador->nome = $data['nome'];
        $prestador->cpf = $data['cpf'];
        $prestador->rg = $data['rg'];
        $prestador->rg_orgao_emissor = $data['rg_orgao_emissor'];
        $prestador->pis_nit = $data['pis_nit'];
	    $prestador->nome_mae = $data['nome_mae'];
	    $prestador->inscricao_municipal = $data['inscricao_municipal'];
        $prestador->cep = $data['cep'];
        $prestador->cidade_id = $data['cidade'];
        $prestador->endereco = $data['endereco'];
        $prestador->numero = $data['numero'];
        $prestador->bairro = $data['bairro'];
        $prestador->telefone = $data['telefone'];
        $prestador->email = $data['email'];
        $prestador->data_nascimento = $data['data_nascimento'];

        $responsavelId = Auth::user()->id;
        $empresa = Empresa::where('responsavel_id', $responsavelId)->first();

        $prestador->empresa_id = $empresa->id;

        $prestador->save();

        $status = 'O prestador de serviço ' . $prestador['nome'] . ' foi cadastrado com sucesso.';

        return redirect(route('prestadores_servicos.index'))->with('status', $status);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prestador = PrestadorServico::findOrfail($id);
        $cidades = \App\Model\Cidade::where('estado_id', 22)->get();

        return view('app.prestadores_servicos.edit', compact('prestador', 'cidades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'nome' => 'required|string|max:255',
            'cpf' => 'required|string|size:11',
            'rg' => 'required|string|max:8',
            'rg_orgao_emissor' => 'required|string|max:8',
            'inscricao_municipal' => 'required|string|size:11',
            'pis_nit' => 'nullable|string|min:11',
            'nome_mae' => 'nullable|string',
            'cep' => 'required|string|size:8',
            'cidade' => 'required|integer',
            'endereco' => 'required|string|max:255',
            'numero' => 'required|string|max:6',
            'bairro' => 'required|string|max:255',
            'telefone' => 'required|string|between:10,11',
            'email' => 'required',
            'data_nascimento' => 'required',
            'renda_eclesiatica' => 'required|boolean',
        ]);

        $prestador = PrestadorServico::findOrfail($id);

        $prestador->nome = $data['nome'];
        $prestador->cpf = $data['cpf'];
        $prestador->rg = $data['rg'];
        $prestador->rg_orgao_emissor = $data['rg_orgao_emissor'];
        $prestador->inscricao_municipal = $data['inscricao_municipal'];
        $prestador->pis_nit = $data['pis_nit'];
        $prestador->cep = $data['cep'];
        $prestador->cidade_id = $data['cidade'];
        $prestador->endereco = $data['endereco'];
        $prestador->numero = $data['numero'];
        $prestador->bairro = $data['bairro'];
        $prestador->telefone = $data['telefone'];
        $prestador->email = $data['email'];
        $prestador->data_nascimento = $data['data_nascimento'];
        $prestador->renda_eclesiatica = $data['renda_eclesiatica'];

        $prestador->save();

        $status = 'O prestador de serviço ' . $prestador['nome'] . ' foi atualizado com sucesso.';

        return redirect(route('prestadores_servicos.index'))->with('status', $status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
