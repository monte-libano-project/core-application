<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Model\Empresa;
use App\Model\PrestadorServico;
use App\Services\Contratos\RpaService;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use PDF;

class ContratoController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $responsavelId = Auth::user()->id;
        $empresa = Empresa::where('responsavel_id', $responsavelId)->first();
        $prestadores = PrestadorServico::where('empresa_id', $empresa->id)->get();

        return view('app.contratos.index', compact('empresa', 'prestadores'));
    }

    public function create(Request $request)
    {
        $data = $request->validate([
            'tipo_contrato' => 'required|string',
            'servico' => 'required|string',
            'valor_bruto' => 'required|numeric',
            'empresa_id' => 'required|integer',
            'prestador_id' => 'required|integer'
        ]);

        $tipoContrato = $data['tipo_contrato'];
        $servico = $data['servico'];
        $valorBruto = floatval($data['valor_bruto']);

        $empresa = Empresa::find($data['empresa_id']);
        $prestador = PrestadorServico::find($data['prestador_id']);

        $cidade = \App\Model\Cidade::find($prestador->cidade_id);
        $estado = \App\Model\Estado::find($cidade->estado_id);

        $descontoISS = RpaService::calculateDescontoISS($valorBruto);

        if ($prestador->renda_eclesiatica) {
            $descontoINSS = 0;
            $baseCalculoIRPF = $valorBruto;
        } else {
            $descontoINSS = RpaService::calculateDescontoINSS($valorBruto);
            $baseCalculoIRPF = $valorBruto - $descontoINSS;
        }

        list($aliquotaIRPF, $deducaoIRPF) = RpaService::getAliquotaAndDeducaoFromTabelaIR($baseCalculoIRPF);

        $descontoIRPF = RpaService::calculateDescontoIRPF($baseCalculoIRPF, $aliquotaIRPF, $deducaoIRPF);

        $valorLiquido = $valorBruto - ($descontoISS + $descontoINSS + $descontoIRPF);

        $now = new Carbon();

        $dia = $now->day;
        $mes = \App\Helpers\DateHelpers::getMonthName($now->month);
        $ano = $now->year;

        // return view('app.contratos.' . $tipoContrato, compact(
        //     'empresa', 'prestador', 'cidade', 'estado', 'dia', 'mes', 'ano'
        // ));

        $pdf = PDF::loadView('app.contratos.' . $tipoContrato, compact(
            'empresa',
            'prestador',
            'servico',
            'valorBruto',
            'descontoISS',
            'descontoINSS',
            'descontoIRPF',
            'aliquotaIRPF',
            'deducaoIRPF',
            'valorLiquido',
            'cidade',
            'estado',
            'dia',
            'mes',
            'ano'
        ));

        return $pdf->stream('document.pdf');
    }
}
