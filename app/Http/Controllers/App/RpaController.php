<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Model\Empresa;
use App\Model\PrestadorServico;
use Illuminate\Support\Facades\Request;

class RpaController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        // $empresa = \App\Model\Empresa::id();

        // $empresa = Empresa::find(Request::input('id'));
        $empresa = Empresa::find('1');
        $prestador = PrestadorServico::find('1');

        return view('app.contratos.rpa', compact('empresa', 'prestador'));
    }
}
