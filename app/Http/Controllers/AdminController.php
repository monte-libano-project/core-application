<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index()
    {
        if (Auth::user()->hasRole('admin')) {
            $empresasCount = \App\Model\Empresa::all()->count();
            $usuariosCount = \App\Model\Usuario::all()->count();

            return view('admin.dashboard', compact('empresasCount', 'usuariosCount'));
        } else {
            return redirect(\App\Providers\RouteServiceProvider::HOME);
        }
    }
}
