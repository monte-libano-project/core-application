<?php

namespace App\Http\Controllers\Admin;

use App\Model\AliquotaImpostoRenda;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AliquotaImpostoRendaController extends Controller
{
    public function index(Request $request)
    {
        $aliquotas = AliquotaImpostoRenda::all();

        return view('admin.aliquota_imposto_renda.index', compact('aliquotas'));
    }

    public function edit($id)
    {
        $aliquota = AliquotaImpostoRenda::findOrfail($id);

        return view('admin.aliquota_imposto_renda.edit', compact('aliquota'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'faixa_inicio' => 'required|numeric',
            'faixa_fim' => 'required|numeric',
            'aliquota' => 'required|numeric',
            'deducao' => 'required|numeric'
        ]);

        AliquotaImpostoRenda::findOrfail($id)->update($data);

        $status = 'A aliquota foi atualizada com sucesso.';

        return redirect(route('aliquota_imposto_renda.index'))->with('status', $status);
    }
}
