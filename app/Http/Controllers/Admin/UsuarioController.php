<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UsuarioController extends Controller
{
    protected $perPage = 20;

    /**
     * Display a paginated listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = $this->perPage;
        $page = $request->page ? $request->page : 1;
        $usuarios = Usuario::paginate($this->perPage);

        return view('admin.usuarios.index', compact('usuarios', 'page', 'perPage'))->with('query', $request->q);
    }

    /**
     * Display a filtered paginated listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $perPage = $this->perPage;
        $page = $request->page ? $request->page : 1;
        $query = '%' . $request->q . '%';

        $usuarios = Usuario::where('email', 'LIKE', $query)
            ->orWhere('nome', 'LIKE', $query)
            ->paginate($this->perPage);

        return view('admin.usuarios.index', compact('usuarios', 'page', 'perPage'))->with('query', $request->q);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rolesNames = Role::all()->pluck('name')->toArray();
        $roles = array_combine($rolesNames, $rolesNames);

        return view('admin.usuarios.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'nome' => 'required|string|max:255',
            'telefone' => 'nullable|string|between:10,11',
            'email' => 'required|string',
            'senha' => 'required|string|min:6'
        ]);

        $data['password'] = Hash::make($data['senha']);
        $data['email_verified_at'] = new \DateTime();

        unset($data['senha']);

        $usuario = Usuario::create($data);
        $usuario->assignRole($request->role_name);

        $status = 'O usuário ' . $usuario->nome . ' foi cadastrado com sucesso.';

        return redirect(route('usuarios.index'))->with('status', $status);
    }

    public function edit($id)
    {
        $usuario = Usuario::findOrfail($id);

        return view('admin.usuarios.edit', compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->validate([
            'nome' => 'required|string|max:255',
            'email' => 'required|string',
            'telefone' => 'nullable|string|between:10,11',
            'senha' => 'nullable|string|min:6'
        ]);

        if ($data['senha']) {
            $data['password'] = Hash::make($data['senha']);
        }

        unset($data['senha']);

        $usuario = Usuario::findOrfail($request->usuario)->update($data);

        $status = 'O usuario ' . $usuario['nome'] . ' foi atualizado com sucesso.';

        return redirect(route('usuarios.index'))->with('status', $status);
    }

    public function destroy($id)
    {
        $usuario = Usuario::findOrfail($id);

        $usuario->delete();

        return redirect(route('usuarios.index'));
    }
}
