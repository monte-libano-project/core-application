<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Empresa;
use App\Model\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class EmpresaController extends Controller
{
    protected $perPage = 20;

    /**
     * Display a paginated listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = $this->perPage;
        $page = $request->page ? $request->page : 1;
        $empresas = Empresa::paginate($this->perPage);

        return view('admin.empresas.index', compact('empresas', 'page', 'perPage'))->with('query', $request->q);
    }

    /**
     * Display a filtered paginated listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $perPage = $this->perPage;
        $page = $request->page ? $request->page : 1;
        $query = '%' . $request->q . '%';

        $empresas = Empresa::where('cnpj', 'LIKE', $query)
            ->orWhere('nome', 'LIKE', $query)
            ->paginate($this->perPage);

        return view('admin.empresas.index', compact('empresas', 'page', 'perPage'))->with('query', $request->q);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cidades = \App\Model\Cidade::where('estado_id', 22)->get();

        return view('admin.empresas.create', compact('cidades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'cnpj' => 'required|string|unique:empresas|size:14',
            'nome' => 'required|string|max:255',
            'cep' => 'required|string|size:8',
            'endereco' => 'required|string|max:255',
            'numero' => 'required|string|max:6',
            'bairro' => 'required|string|max:255',
            'cidade_id' => 'required|integer',
            'responsavel' => 'required|string|max:255',
            'telefone' => 'required|string|between:10,11',
            'email' => 'required|string',
            'senha' => 'required|string|min:6'

        ]);

        $usuarioData = [
            'nome' => $data['responsavel'],
            'email' => $data['email'],
            'telefone' => $data['telefone'],
            'password' => Hash::make($data['senha'])
        ];

        $usuario = Usuario::create($usuarioData);

        $data['responsavel_id'] = $usuario->id;

        $empresa = Empresa::create($data);

        $status = 'A empresa ' . $empresa->nome . ' foi cadastrada com sucesso.';

        return redirect(route('empresas.index'))->with('status', $status);
    }

    public function edit($id)
    {
        $empresa = Empresa::findOrfail($id);
        $cidades = \App\Model\Cidade::where('estado_id', 22)->get();
        $usuarios = \App\Model\Usuario::all();

        return view('admin.empresas.edit', compact('empresa', 'cidades', 'usuarios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->validate([
            'cnpj' => 'required|string|size:14',
            'nome' => 'required|string|max:255',
            'cep' => 'required|string|size:8',
            'endereco' => 'required|string|max:255',
            'numero' => 'required|string|max:6',
            'bairro' => 'required|string|max:255',
            'cidade_id' => 'required|integer',
            'responsavel_id' => 'required|integer'
        ]);

        $empresa = Empresa::findOrfail($request->empresa)->update($data);

        $status = 'A empresa ' . $empresa['nome'] . ' foi atualizada com sucesso.';

        return redirect(route('empresas.index'))->with('status', $status);
    }

    public function destroy($id)
    {
        $empresa = Empresa::findOrfail($id);

        $empresa->delete();

        return redirect(route('empresas.index'));
    }
}
