<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AliquotaImpostoRenda extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'aliquota_imposto_renda';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['faixa_inicio', 'faixa_fim', 'aliquota', 'deducao'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
}
