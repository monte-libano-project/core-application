<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PrestadorServico extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'prestadores_servicos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
}
